/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.crossengage.app;

import org.jsmpp.DefaultPDUReader;
import org.jsmpp.DefaultPDUSender;
import org.jsmpp.InvalidResponseException;
import org.jsmpp.PDUException;
import org.jsmpp.SynchronizedPDUSender;
import org.jsmpp.bean.BindType;
import org.jsmpp.bean.DataCodings;
import org.jsmpp.bean.ESMClass;
import org.jsmpp.bean.NumberingPlanIndicator;
import org.jsmpp.bean.OptionalParameter;
import org.jsmpp.bean.OptionalParameters;
import org.jsmpp.bean.RegisteredDelivery;
import org.jsmpp.bean.SMSCDeliveryReceipt;
import org.jsmpp.bean.TypeOfNumber;
import org.jsmpp.extra.NegativeResponseException;
import org.jsmpp.extra.ResponseTimeoutException;
import org.jsmpp.session.BindParameter;
import org.jsmpp.session.SMPPSession;
import org.jsmpp.util.AbsoluteTimeFormatter;
import org.jsmpp.util.DefaultComposer;
import org.jsmpp.util.TimeFormatter;

import java.io.IOException;
import java.util.Date;
import java.util.Random;

public class SubmitLongMessageExample {
    private static TimeFormatter timeFormatter = new AbsoluteTimeFormatter();

    public static void main(String[] args) {
        String destination = args[0];
        String systemId = args[1];
        String password = args[2];
        sendMessagePartsExample(systemId, password, destination);
    }

    public static void sendMessagePartsExample(String systemId, String password, String destination) {
        SMPPSession session = new SMPPSession(
                new SynchronizedPDUSender(new DefaultPDUSender(new DefaultComposer())),
                new DefaultPDUReader(),
                XngSSLSocketConnectionFactory.getInstance());
        try {
            final BindParameter bindParam = new BindParameter(BindType.BIND_TX, systemId, password, "cp", TypeOfNumber.UNKNOWN, NumberingPlanIndicator.UNKNOWN, null);
            session.connectAndBind("ham.smpp.api.linkmobility.de", 4002, bindParam, 10000);
            System.out.println("connected to SMSC");
        } catch (IOException e) {
            System.err.println("Failed connect and bind to host");
            e.printStackTrace();
            session.unbindAndClose();
            return;
        }

        Random random = new Random();

        try {
            final int totalSegments = 4;
            short refNumAsShort = (short) random.nextInt();
            OptionalParameter sarMsgRefNum = OptionalParameters.newSarMsgRefNum(refNumAsShort);
            OptionalParameter sarTotalSegments = OptionalParameters.newSarTotalSegments(totalSegments);

            System.out.println("sending messages...");
            for (int i = 0; i < totalSegments; i++) {
                final int seqNum = i + 1;
                String message = "Ref: " + refNumAsShort + " - Message part " + seqNum + " of " + totalSegments + " ";
                OptionalParameter sarSegmentSeqnum = OptionalParameters.newSarSegmentSeqnum(seqNum);
                System.out.println("sending message " + i);
                String messageId = submitMessage(destination, session, message, sarMsgRefNum, sarSegmentSeqnum, sarTotalSegments);
                System.out.println("Message submitted, message_id is " + messageId);
            }

        } finally {
            session.unbindAndClose();
        }
    }

    public static String submitMessage(String destination, SMPPSession session, String message, OptionalParameter
            sarMsgRefNum, OptionalParameter sarSegmentSeqnum, OptionalParameter sarTotalSegments) {
        String messageId = null;
        try {
            messageId = session.submitShortMessage("CMT", TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.UNKNOWN, "1616",
                    TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.UNKNOWN, destination, new ESMClass(), (byte) 0, (byte) 1,
                    timeFormatter.format(new Date()), null, new RegisteredDelivery(SMSCDeliveryReceipt.DEFAULT), (byte) 0, DataCodings.ZERO, (byte) 0,
                    message.getBytes(), sarMsgRefNum, sarSegmentSeqnum, sarTotalSegments);
            ;
        } catch (PDUException e) {
            // Invalid PDU parameter
            System.err.println("Invalid PDU parameter");
            e.printStackTrace();
        } catch (ResponseTimeoutException e) {
            // Response timeout
            System.err.println("Response timeout");
            e.printStackTrace();
        } catch (InvalidResponseException e) {
            // Invalid response
            System.err.println("Receive invalid response");
            e.printStackTrace();
        } catch (NegativeResponseException e) {
            // Receiving negative response (non-zero command_status)
            System.err.println("Receive negative response");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("IO error occur");
            e.printStackTrace();
        }
        return messageId;
    }
}