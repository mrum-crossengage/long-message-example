package io.crossengage.app;

import org.jsmpp.session.connection.Connection;
import org.jsmpp.session.connection.ConnectionFactory;
import org.jsmpp.session.connection.socket.SocketConnection;

import java.io.IOException;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

// For some reason we now have to implement our own SSLSocketConnectionFactory,
// since SSLSocketConnectionFactory getInstance method was removed and constructor is private...
public class XngSSLSocketConnectionFactory implements ConnectionFactory {

    // Holder class for lazy singleton
    private static class Holder {
        private static final XngSSLSocketConnectionFactory INSTANCE = new XngSSLSocketConnectionFactory();
    }

    private final SocketFactory socketFactory;

    private XngSSLSocketConnectionFactory() {
        socketFactory = SSLSocketFactory.getDefault();
    }

    public static XngSSLSocketConnectionFactory getInstance() {
        return Holder.INSTANCE;
    }

    @Override
    public Connection createConnection(String host, int port)
            throws IOException {
        return new SocketConnection(socketFactory.createSocket(host, port));
    }
}