# Example for submitting an SMS in multiple parts

## Run it
```
> mvn clean package
> java -jar target/long-message-example-1.0-SNAPSHOT-jar-with-dependencies.jar "<destination-phone-number>" "<your-system-id>" "<your-password>"
```